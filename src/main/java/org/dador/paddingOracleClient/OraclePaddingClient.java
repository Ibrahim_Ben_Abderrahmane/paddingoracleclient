package org.dador.paddingOracleClient;


import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Main Class for Padding OracleClient
 */
public class OraclePaddingClient {
    static final String ENCRYPTED_MESSAGE = "5ca00ff4c878d61e1edbf1700618fb287c21578c0580965dad57f70636ea402fa0017c4acc82717730565174e2e3f713d3921bab07cba15f3197b87976525ce4";
    static final int BLOCK_SIZE = 16;

    /**
     * Fonction takes a number and creates a block of x00 values, padded according to PKCS#7
     * example : n=3 result is 00 00 .. 00 03 03 03
     * @param n : number of bytes of padding
     * @return byte[BLOCK_SIZE] filled with 0 and padding values
     */
    protected byte[] getPaddingArray(int n) {
        byte[] result = new byte[BLOCK_SIZE];
        for (int i=BLOCK_SIZE-n;i<BLOCK_SIZE;i++)
        {
        		result[i] =  (byte)n;
        }
        return result;
    }

    /**
     * Function that create a modified ciphertext bloc for trying a guess
     * Note that the "ciphertext" correspond to the IV part for the Block Cipher
     * @param ciphertext : original ciphertext bloc
     * @param decoded    : decrypted part of the plain text (for next bloc)
     * @param position   : position of the byte to guess
     * @param guess      : the guess for this query
     * @return a byte array with c0...c(i-1)||ci+i+g||cj+mj+i||...||cn+mn+i
     */
    protected byte[] buildGuessForPosition(byte[] ciphertext, byte[] decoded, int position, byte guess) {
        byte[] result = new byte[BLOCK_SIZE];
        byte[] xor_mess_padd= HexConverters.xorArray(ciphertext,getPaddingArray(1));
        //int taille=xor_mess_padd.length;
        xor_mess_padd[position] = (byte) (xor_mess_padd[position] ^ guess);
        result = xor_mess_padd;
        return result;
    }

    /**
     * Fonction that splits a message into constituent blocs of BLOCK_SIZE
     *
     * @param message
     * @return an array of blocs
     * @throws IllegalArgumentException
     */
    protected byte[][] splitMessageIntoBlocks(byte[] message) throws IllegalArgumentException {
        if (message.length % BLOCK_SIZE != 0) {
            throw new IllegalArgumentException("Message length is not a multiple of bloc size");
        }
        /**
         * TODO : YOUR CODE HERE
         */
        int taille = message.length;
        int i;
        int j;
        byte [][] b = new byte[taille/BLOCK_SIZE][BLOCK_SIZE];

        for (i=0;i<taille/BLOCK_SIZE;i++)
        {
            for (j=0;j<BLOCK_SIZE;j++)
        	{
        		b[i][j] = message[i*BLOCK_SIZE+j];
        	}
        }
        
        return b;
    }

    /**
     * Function that takes the 2 last blocks of the message
     * and returns the length of the padding.
     * @param poq : a PaddingOracleQuery object
     * @param previousbloc : next to last block of the ciphertext
     * @param lastbloc : last bloc of the ciphertext
     * @return an integer corresponding to padding length
     * @throws IOException
     * @throws URISyntaxException
     */
    public int getPaddingLengthForLastBlock(PaddingOracleQuery poq, byte[] previousbloc, byte[] lastbloc) throws IOException, URISyntaxException {
    	System.out.println(HexConverters.getStringHexRepresentationFromByteArray(previousbloc));
    	byte [] Concaten=HexConverters.getByteArrayFromStringHexRepresentation(
    			HexConverters.getStringHexRepresentationFromByteArray(previousbloc)
    			+HexConverters.getStringHexRepresentationFromByteArray(lastbloc)
    			);
    	int padd = Concaten[Concaten.length-1];
    	int padd_length=0;
    	for(int i=Concaten.length-1;i>0;i--)
    	{
    		if (Concaten[i] == (byte)padd)  
    			padd_length++;
    	}
        // should not arrive here !
        return padd_length;
    }

    /**
     * Main function that takes 2 consecutive blocks of the ciphertext
     * and returns the decryption of the 2nd message block
     *
     * @param poq : a PaddingOracleQuery object to query server
     * @param iv : the "iv" part of the 2 blocks query
     * @param ciphertext : the block that will be decrypted
     * @param padding : set to 0 if not the last block. Set to paddinglength if last block
     * @return a decrypted byte array
     * @throws IOException
     * @throws URISyntaxException
     */
    public byte[] runDecryptionForBlock(PaddingOracleQuery poq, byte[] iv, byte[] ciphertext, int padding) throws IOException, URISyntaxException {
        byte[] decoded = new byte[BLOCK_SIZE];
        if (padding > 0) {
            decoded = getPaddingArray(padding);
        }
        byte[] ivv = new byte[BLOCK_SIZE];
        int position =15;
        int guess = 0;
        for(guess=0; guess<255; guess+=1)
        {
        	
        	System.out.println("guess "+guess);
        	ivv = buildGuessForPosition(iv, decoded, position, (byte)guess);
        	String str = HexConverters.getStringHexRepresentationFromByteArray(ivv)
        			+HexConverters.getStringHexRepresentationFromByteArray(ciphertext);
        	boolean b = poq.query(str);
        	System.out.println(poq.query(str));
        	if (b == true) break;
        }
        
        return decoded;
    }

    public static void main(String[] args) {
        OraclePaddingClient opc = new OraclePaddingClient();
        PaddingOracleQuery opq = new PaddingOracleQuery();
        try {
            System.out.println("Server responded : " + opq.query(ENCRYPTED_MESSAGE));
        } catch (Exception e) {
            System.out.print("Exception caught. Server down ?");
            e.printStackTrace();
        }
        
        try {
        	byte[] mess = HexConverters.getByteArrayFromStringHexRepresentation(ENCRYPTED_MESSAGE);
        	byte[][] decoupe = opc.splitMessageIntoBlocks(mess);
        	for (int i=0;i<decoupe.length;i++)
        	System.out.println(HexConverters.getStringHexRepresentationFromByteArray(decoupe[i]));
        	opc.runDecryptionForBlock(opq, decoupe[1] , decoupe[2], 0);
        	opc.getPaddingLengthForLastBlock(opq,decoupe[1],decoupe[2]);
        } catch (Exception e) {
            System.out.print("Exception caught. Server down ?");
            e.printStackTrace();
        }
    }

}

